import React from 'react'
import "./Header.scss"

// IMPORT IMAGES
import Logo from "./img/Logo.svg"


const Header = () => {
    const itemsInCart = 1;
    
    return (
        <header className="header container">
            <div className="header__upper">
                <nav className="header__nav">
                    <ul className="nav__list">
                        <li className="nav__item"><a href="error/404">Магазины</a></li>
                        <li className="nav__item"><a href="error/404">Акции</a></li>
                        <li className="nav__item"><a href="error/404">Доставка и оплата</a></li>
                    </ul>
                </nav>
                <div className="header__logo">
                    <a href="error/404"><img src={Logo} alt="DriveMotoLogo" /></a>
                </div>
                <div className="header__address">
                    Москва,  ул. Науки  25
                </div>
                <nav className="header__personal">
                    <ul className="nav__list">
                        <li className="nav__item header__favorites">
                            <a className="personal__link" href="error/404"></a>
                        </li>
                        <li className="nav__item header__profile">
                            <a className="personal__link" href="error/404"></a>
                        </li>
                        <li className="nav__item header__cart">
                            <a className="personal__link cart__link" href="error/404">{itemsInCart}</a>
                            <a className="personal__link cart__icon" href="error/404"></a>
                            
                        </li>
                    </ul>
                </nav>
            </div>
            <div className="header__under">
                <nav className="header__category">
                    <ul className="nav__list">
                        <li className="nav__item"><a href="error/404">Квадроциклы</a>
                        </li>
                        <li className="nav__item">
                            <a href="error/404">Катера</a>
                        </li>
                        <li className="nav__item">
                            <a href="error/404">Гидроциклы</a>
                        </li>
                        <li className="nav__item">
                            <a href="error/404">Лодки</a>
                        </li>
                        <li className="nav__item">
                            <a href="error/404">Вездеходы</a>
                        </li>
                        <li className="nav__item">
                            <a href="error/404">Снегоходы</a>
                        </li>
                        <li className="nav__item">
                            <a href="error/404">Двигатели</a>
                        </li>
                        <li className="nav__item">
                            <a href="error/404">Запчасти</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
    )
}

export default Header